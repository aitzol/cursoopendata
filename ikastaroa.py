import io
import requests
import xml.etree.ElementTree as etree
from collections import defaultdict
import scodeu
client = scodeu.ProcedimientosAdministrativos()

def get_contrataciones():
    tipo = 'anuncio_contratacion'
    metadata = (('documentLanguage', 'EQ', 'es'),
                ('documentCreateDate','BETWEEN', '01/01/2015,31/12/2015'),
                )
    ordering = (('contratacion_fecha_de_publicacion_document0', 'DESC'),)
    data = client.codified_search(
        tipo=tipo,
        metadata=metadata,
        ordering=ordering
        )
    items = data['items']
    total = data.get('total')
    cont = data.get('more', None)
    print('{0} of {1} items fetched. Next page: {2}'.format(len(items), total, cont))
    while cont is not None:
        data = client.codified_search(
            tipo=tipo,
            metadata=metadata,
            ordering=ordering,
            page = cont
        )
        if len(data['items']) == 0:
            break # search stops returning items on 5000
        items.extend(data['items'])
        cont = data.get('more', None)
        print('{0} of {1} items fetched. Next page: {2}'.format(len(items), total, cont))
    return items
    
def get_content(item):
    file_url_template = 'http://opendata.euskadi.eus/contenidos/anuncio_contratacion/{0}/es_doc/data/es_{1}'
    contentName = item.get('contentName')
    datafileOids = item.get('datafileOids')
    return [file_url_template.format(contentName, datafileOid) for datafileOid in datafileOids]

if __name__ == '__main__':
    suma = defaultdict(float)
    items = get_contrataciones()
    for item in items:
        for url in get_content(item):
            print('Processing: {0}'.format(url))
            content = requests.get(url)
            if content.status_code == 200 and content.text != '':
                try:
                    tree = etree.parse(io.StringIO(content.text))
                except etree.ParseError:
                    # urls doesn't contain any XML
                    continue
                    
                presupuesto = tree.find(".//*[@name='contratacion_presupuesto_contrato_cab']")
                entidad = tree.find(".//*[@name='contratacion_poder_adjudicador']")
                entidad_nombre = entidad.find(".//*[@name='valor']")
                if presupuesto:
                    # dirty conversion
                    original = presupuesto.getchildren()[0].text
                    original = original.replace('.','').replace(',','.')
                    suma[entidad_nombre.getchildren()[0].text] += float(original)
    for k,v in suma.items():
        print('{0}\t\t{1}'.format(k, v))
                                       
                

